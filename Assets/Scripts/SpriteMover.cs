﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMover : MonoBehaviour {
    // public so you can acess it in unity
	private Transform tf;// variable to hold the transform 
	public float speed;
	// Use this for initialization
	void Start ()
    {
		tf = GetComponent<Transform> ();
        // sets the speed 
        speed = .1f;
	}
	
	// Update is called once per frame
	void Update ()
	{
        //if shift is pressed then if its your first time pressing the arrow it will move
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                Debug.Log("Shift Up");
                tf.transform.Translate(Vector3.up);
            }
        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            // says up in the debug when i press w
            Debug.Log("Up");
            //moves it in the positive y direction * the speed so i can controll how fast
            tf.transform.Translate(Vector3.up * speed);
        }


        //if shift is pressed then if its your first time pressing the arrow it will move
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                Debug.Log("Shift Down");
                tf.transform.Translate(Vector3.down);
            }
        }
        else if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow))
        {
            // says down in the debug when i press s
            Debug.Log ("Down");
            //moves it in the negative y direction * the speed so i can controll how fast
			tf.transform.Translate (Vector3.down * speed);
		}


        //if shift is pressed then if its your first time pressing the arrow it will move
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Debug.Log("Shift left");
                tf.transform.Translate(Vector3.left);
            }
        }
        else if (Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            // says left in the debug when i press a
            Debug.Log ("Left");
            //moves it in the negative x direction * the speed so i can controll how fast
            tf.transform.Translate (Vector3.left * speed);
		}


        //if shift is pressed then if its your first time pressing the arrow it will move
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                Debug.Log("Shift Right");
                tf.transform.Translate(Vector3.right);
            }
        }
        else if (Input.GetKey (KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            // says right in the debug when i press d
            Debug.Log ("Right");
            //moves it in the positive x direction * the speed so i can controll how fast
            tf.transform.Translate (Vector3.right * speed);
		}
        

        if (Input.GetKey (KeyCode.Space)) {
           
            Debug.Log ("Space");
            // sets the position back to 0 0 0
			tf.transform.position = new Vector3 (0, 0, 0);
		}

      
		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit ();
		}
	}
}
