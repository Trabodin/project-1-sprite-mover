﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deacivator : MonoBehaviour {
    public GameObject spriteMoverObj;
    
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        // i made it get key down so it wouldnt keep turning off and on if u held it own
        if (Input.GetKeyDown(KeyCode.P))
        {  
            
            if (GetComponent<SpriteMover>().enabled == true)
            {
                GetComponent<SpriteMover>().enabled = false;
            }

            else if (GetComponent<SpriteMover>().enabled == false)
            {
                GetComponent<SpriteMover>().enabled = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {

            if (spriteMoverObj.activeSelf == true)
            {
                spriteMoverObj.SetActive(false);

            }
        }
    }
}
