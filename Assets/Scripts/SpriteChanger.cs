﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {
    // set the sprite color in unity
    public Color spriteColor;
    //get the renderer in unity
    public SpriteRenderer therenderer;
 

	// Use this for initialization
	void Start () {
        
        spriteColor.a = 1.0f;
        // when it renders set the color to sprite color
        if (therenderer != null)
            therenderer.color = spriteColor;

	}
	
	// Update is called once per frame
	void Update () {
		


	}
}
